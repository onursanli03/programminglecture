package com.ieeeytu.Arrays;

public class OneDimensionalArrays {

    public static void main(String[] args) {

        int arr [] = {1,2,3,4,5,6,7,8,9};

        int [] arr2 = new int[100];

        for (int i = 0; i<arr2.length;i++){

            arr2[i]=(i*2)+1;

            System.out.println(arr2[i]);
        }

        System.out.println(" /* -------------------------------- */ ");

        for (int i = 0; i<arr.length;i++){
            System.out.println(arr[i]);
        }

    }
}
