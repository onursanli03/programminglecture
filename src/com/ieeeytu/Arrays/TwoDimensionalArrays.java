package com.ieeeytu.Arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {

    public static void main(String[] args) {

        int [][] arr = {{1,2,3},{4,5,6},{7,8,9}};


        for (int i = 0; i < arr.length; i++){
            for( int j = 0; j < arr.length; j++){
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.print("\n");
        System.out.print("\n");

        for(int [] mat : arr){
            System.out.println(Arrays.toString(mat));
        }

    }
}
