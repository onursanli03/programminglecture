package com.ieeeytu.DataTypes;

import java.util.Scanner;

public class FinalAndStatic {

    final static int b = 4;

    static{
        String str1 = "foo";
        System.out.println(str1);
    }

    public static void main(String[] args) {

        final int a = 5;
        A aa = new A();
        aa.sayHello();
        staticWrite();
        FinalAndStatic fs = new FinalAndStatic();
        fs.finalWrite();
        B b = new B();
        b.foo();



    }

    public static class A {
        public void sayHello(){
            System.out.println("Hello!");
        }
    }

    public static final class B{

        public void foo(){
            System.out.println("bar");
        }

    }

    public final void finalWrite(){
        System.out.println("foo");
    }

    public static void staticWrite(){
        System.out.println("bar");
    }


}
