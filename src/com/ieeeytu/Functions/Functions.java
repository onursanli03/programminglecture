package com.ieeeytu.Functions;

public class Functions {

    public static void yazdir(){
        System.out.println("Parametresiz Fonksiyon");
    }

    public static void yazdir(String str){
        System.out.println(str);
    }

    public static int carp(int a, int b){
        return a * b;
    }

    public static double topla(double a, double b){
        return a + b;
    }

    static void drawCircle(int radius) {

        double dist;

        for (int i = 0; i <= 2 * radius; i++) {


            for (int j = 0; j <= 2 * radius; j++) {
                dist = Math.sqrt((i - radius) * (i - radius) +
                        (j - radius) * (j - radius));

                if (dist > radius - 0.5 && dist < radius + 0.5)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.print("\n");
        }
    }

    public static void main(String[] args) {

        yazdir();

        yazdir("Bu bir parametreli fonksiyon");

        System.out.println(carp(2,4));

        System.out.println(topla(3.0,5.0));

        int sayi1 = carp(8,9);
        double sayi2 = topla(6.0,7.0);

        System.out.println(sayi1 + "\t" + sayi2);

        drawCircle(5);

    }

}
