package com.ieeeytu.HelloWorld; // Paketler

import java.util.Scanner; //Library'ler

public class HelloWorld { // Class

    public static Scanner sc = new Scanner(System.in); // Global variable

    public static void main(String[] args) { // Method

        System.out.println("Hello, world!"); // Console'a " Hello, world! " yazdıracak.

        String username,password; // Local variable

        username = sc.nextLine();
        System.out.println("username : " + username);
        password = sc.next();
        System.out.println("password : " + password);

        sc.close(); // Kullanıcıdan veri alımı durduruldu.

        System.out.println("foo"); // Console'a çıktıyı yazdırır ve bir sonraki satıra geçer.
        System.out.print("bar"); // Console'a çıktıyı yazdırır ve mevcut satırda kalır.
        System.err.println("foo"); // Error yazdırır ve bir sonraki satıra geçer.
        System.err.println("bar"); // Error yazdırır ve mevcut satırda kalır.

        // bu bir yorum satırıdır.

        /*
        * Bu
        * bir
        * çok
        * satırlı
        * yorum
        * bloğudur.
        * */

    }

}
