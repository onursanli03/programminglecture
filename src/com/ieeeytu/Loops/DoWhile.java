package com.ieeeytu.Loops;

import java.util.ArrayList;
import java.util.List;

public class DoWhile {


    public static void main(String[] args) {

        int [] arr = {1,2,3,4,5,6,7};
        int count = 0;

        do {
            System.out.println(arr[count]);
            count++;
        }while(count != 7);

        System.out.println("/*-----------------------------*/");

        List<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(232);
        list.add(15);
        list.add(49);
        list.add(91);
        list.add(87);
        list.add(345);

        int count2 = 0;
        while(count2 != 7){
            System.out.println(list.get(count2));
            count2++;
        }

    }



}
