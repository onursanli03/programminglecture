package com.ieeeytu.Loops;

import java.util.LinkedList;
import java.util.List;

public class ForEachLoops {

    public static void main(String[] args) {

        List<String> items = new LinkedList<>();

        items.add("Kalem");
        items.add("Kitap");
        items.add("Mouse");
        items.add("Dergi");
        items.add("Klavye");

        List<String> items2 = new LinkedList<>();
        items2.add("Bardak");
        items2.add("Termos");
        items2.add("Kahve");
        items2.add("Defter");
        items2.add("Çanta");

        // for loop ile
        for (String item : items) {
            System.out.println(item);
        }
        System.out.println("/*--------------------------------------------*/");

        items.forEach(item -> System.out.println(item)); // Lambda ile gösterim

        System.out.println("/*--------------------------------------------*/");

        items2.forEach(System.out::println); // Method referansı ile

    }
}
