package com.ieeeytu.Loops;

import java.util.LinkedList;
import java.util.List;

public class ForLoops {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++){
            System.out.println(i);
        }


        List<String> items = new LinkedList<>();

        items.add("Kalem");
        items.add("Kitap");
        items.add("Mouse");
        items.add("Dergi");
        items.add("Klavye");

        for (int i = 0; i<items.size(); i++){
            System.out.println(items.get(i));
        }


        for (int i=1; i<10; i += 2)
        {
            for (int j=0; j<i; j++)
            {
                System.out.print("*");
            }
            System.out.println("");
        }

        for (int i=1; i<10; i += 2)
        {
            for (int k=0; k < (4 - i / 2); k++)
            {
                System.out.print(" ");
            }
            for (int j=0; j<i; j++)
            {
                System.out.print("*");
            }
            System.out.println("");
        }


    }
}
