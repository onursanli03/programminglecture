package com.ieeeytu.Operators;

public class Operators {

    public static void main(String[] args) {

        /* Atama operatörü '=' */

        int a = 5;
        System.out.println(a);

        /* Aritmetik operatörler '+', '-', '*', '/', '%' */

        int b = 2;

        int toplam = a+b;
        System.out.println("Toplama işlemi sonucu : " + toplam);
        int cikarma = a-b;
        System.out.println("Çıkarma işlemi sonucu : " + cikarma);
        int carpma = a*b;
        System.out.println("Çarpma işlemi sonucu : " + carpma);
        int bolme = a/b;
        System.out.println("Bölme işlemi sonucu : " + bolme);
        int mod = a%b;
        System.out.println("Mod Alma işlemi sonucu : " + mod);

        /* Arttırma, azaltma '++', '--' */

        int sayi = 1;
        System.out.println("Başlangıçta sayi : " + sayi);

        sayi++;
        System.out.println("Önce işlemi yap sonra 1 arttır : " + sayi);

        ++sayi;
        System.out.println("Önce 1 arttır sonra işlemi yap : " + sayi);

        --sayi;
        System.out.println("Önce 1 azalt sonra işlemi yap : " + sayi);

        sayi--;
        System.out.println("Önce işlemi yap sonra 1 azalt : " + sayi);

        /* Aritmetik atama operatörleri '+=', '-=', '*=', '/=', '%=' */

        int sayi1 = 32, sayi2 = 48;

        sayi1 += sayi2; // Topla ve ata
        System.out.println("Topla ve ata : " + sayi1); // Beklenen 80

        sayi1 -= sayi2; // Çıkar ve ata
        System.out.println("Çıkar ve ata : " + sayi1); // Beklenen 32

        sayi1 *= sayi2; // Çarp ve ata
        System.out.println("Çarp ve ata : " + sayi1); // Beklenen 1536

        sayi1 /= sayi2; // Böl ve ata
        System.out.println("Böl ve ata : " + sayi1); // Beklenen 32

        sayi1 %= sayi2; // Mod al ve ata
        System.out.println("Mod al ve ata : " + sayi1); // Beklenen 32


        /* Mantıksal Operatörler 'AND', 'OR', 'NOT', 'XOR' */

        int s1 = 10;
        int s2 = 20;

        boolean foo = true;

        System.out.print("AND operatörü : " + (s1 > s2 && s2 > 3) + "\t");
        System.out.println(s1 > s2 & s2 > 3);

        System.out.print("OR operatörü : " + (s1 > s2 || s2 > 3) + "\t");
        System.out.println(s1 > s2 | s1 > 3);

        System.out.println("NOT operatörü : " + !foo);

        System.out.println("XOR operatörü : " + (s1 > s2 ^ s2 > 3));


        /* İlişkisel Operatörler '<', '>', '==', '!=', '<=', '>=' */

        System.out.println(6<7);
        System.out.println(6>7);
        System.out.println(6==7);
        System.out.println(6!=7);
        System.out.println(6<=7);
        System.out.println(6>=7);

        /* Bitwise Operatörler '&', '|', '^', '~', '<<', '>>' */

        int d = 4;
        int e = 7;

        int sonuc = d & e;
        System.out.println("Bitwise AND sonucu : " + sonuc);

        sonuc = d | e;
        System.out.println("Bitwise OR sonucu : " + sonuc);

        sonuc = d ^ e;
        System.out.println("Bitwise XOR sonucu : " + sonuc);

        sonuc = ~d;
        System.out.println("Bitwise Complement sonucu : " + sonuc);

        sonuc = d >> 1;
        System.out.println("Bitwise Shifting sonucu : " + sonuc);

        sonuc = d << 4;
        System.out.println("Bitwise Shifting sonucu : " + sonuc);

        /* ? Operatörü */

        int i = 15;
        int k = 20;
        int l = 30;
        int m = 10;
        int n = i > k  ? l : m;
        System.out.println(n);
        int o = i < k  ? l : m;
        System.out.println(o);



    }
}
