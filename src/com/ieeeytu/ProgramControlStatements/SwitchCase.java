package com.ieeeytu.ProgramControlStatements;

public class SwitchCase {

    public static void main(String[] args) {

        int statusCode = 404;

        switch (statusCode){
            case 1:
                System.out.println("foo");
                break;
            case 2:
                System.out.println("bar1");
                break;
            case 3:
                System.out.println("bar2");
                break;
            case 4:
                System.out.println("bar3");
                break;
            default:
                System.out.println("foo2");

        }



    }
}
