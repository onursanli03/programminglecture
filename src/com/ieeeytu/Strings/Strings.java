package com.ieeeytu.Strings;

public class Strings {

    public static void main(String[] args) {
        String str = "Foo Bar";

        System.out.println(str.substring(1,3));


        System.out.println("Bar baslangici " + str.indexOf("Bar"));


        if (str.equalsIgnoreCase("Foo Bar")){
            System.out.println("Aynı deger");
        }

        System.out.println("Kucuk harfler : " + str.toLowerCase());

        System.out.println("Kucuk harfler : " + str.toUpperCase());


        System.out.println("Bosluklar yok : " + str.trim());

        String[] words = str.split(" ");

        System.out.println("Boşluktan sonraki kelime : " + words[0]);

        /*Byte değer olarak okunan stringi geri stringe çevirme*/
        byte[] strByte = str.getBytes();
        System.out.println(new String(strByte));


        /*charAt*/
        int i;

        for(i=0; i<=str.length()-1; i++) {
            System.out.println("");
            System.out.println(str.charAt(i));
        }

        /*charAt örnek*/
        String str2="IEEE";
        int j;

        j=0;
        System.out.println("KARAKTER"+"   "+"INDEKSI");

        for(j=0; j<=str2.length()-1; j++){
            System.out.println(str2.charAt(j)
                    +"            "
                    +j);
        }


        /*compareTo*/
        String st1="Foo";
        int k;

        k=0;
        k=st1.compareTo("Bar");
        System.out.println(st1);

        if(k==0) {
            System.out.println("");
            System.out.println(st1+" =  "+"Bar");
        }

        if(k>0) {
            System.out.println("");
            System.out.println(st1+" >  "+"Bar");
        }

        if(k<0) {
            System.out.println("");
            System.out.println(st1+"  < "+"Bar");
        }

        System.out.println("i="+k);




        /*getChars*/
        char [] arr = new char[10];

        str.getChars(0,3,arr,0);

        for (int m = 0; m<arr.length;m++){
            System.out.println(arr[m]);
        }


        /*startsWith endsWith*/

        String str1Arr[]={"ankara","antalya","burdur","bursa","anadolu"};
        int n;

        for (n=0; n<str1Arr.length; n++) {
            if(str1Arr[n].startsWith("an")) {
                System.out.println();
                System.out.println(str1Arr[n]+" stringi an  harfleri ile basliyor");
            }
        }
        System.out.println();

        for (int a=0; a<str1Arr.length; a++) {
            if(str1Arr[a].endsWith("a")) {
                System.out.println();
                System.out.println(str1Arr[a]+" stringi a  harfi ile bitiyor");
            }
        }
        System.out.println();

        for (int s=0; s<str1Arr.length; s++) {
            if(str1Arr[s].startsWith("ka",2))
            {
                System.out.println();
                System.out.println(str1Arr[s]+" stringi 2. indisten itibaren ka harfleri ile basliyor");
            }
        }


    }


}
