package com.onur.ders1;

import java.util.Scanner;

public class ControlStatements3 {

    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {


        System.out.println("ilk sayıyı girin");
        int a = sc.nextInt();
        System.out.println("ikinci sayıyı girin");
        int b = sc.nextInt();
        System.out.println("yapacağınız işlemi seçin");
        int secim = sc.nextInt();
        int sonuc = 0;

        switch (secim){
            case 1:
                sonuc = a+b;
                System.out.println("Sonuc: " + sonuc);
                break;
            case 2:
                sonuc = a-b;
                System.out.println("Sonuc: " + sonuc);
                break;
            default:
                System.out.println("yanlış girdii...");
                break;
        }



    }

}
