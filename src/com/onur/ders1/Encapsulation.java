class Employee{

    private String firstname;
    private String lastname;
    private long employeeNumber;
    private String email;
    private String phone;
    
    
    public Employee(){
    }
    
    //getters
    public String getFirstName(){
        return this.firstname;
    }
    
    public String getLastname(){
        return this.lastname;
    }
    
    public long getEmployeeNumber(){
        return this.employeeNumber;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public String getPhone(){
        return this.phone;
    }
    
    //Setters
    
    public void setFirstName(String firstname){
        this.firstname = firstname;
    }
    
    public void setLastName(String lastname){
        this.lastname = lastname;
    }
    
    public void setEmployeeNumber(long employeeNumber){
        this.employeeNumber = employeeNumber;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public void setPhone(String phone){
        this.phone = phone;
    }
    
    @Override
    public String toString(){
        return "Employee Number = " + getEmployeeNumber() + "\n"+
               "Firstname = " + getFirstName() + "\n" +
               "Lastname = " + getLastname() + "\n" +
               "Email = " + getEmail() + "\n" +
               "Phone = " + getPhone();
    }
}

public class Encapsulation{
    
    public static void main(String [] args){
        
        Employee emp = new Employee();
        
        emp.setFirstName("Joe");
        emp.setLastName("Doe");
        emp.setEmployeeNumber(1);
        emp.setEmail("joe.doe@example.com");
        emp.setPhone("123-324-56-89");
        
        System.out.println(emp.toString);
    }
}