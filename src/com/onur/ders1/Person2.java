class Person{

    private String firstname;
    private String lastname;
    private long idNumber;
    
    public Person(String firstname,String lastname,long idNumber){
        
        this.firstname = firstname;
        this.lastname = lastname;
        this.idNumber = idNumber;
                
    }
    
    @Override
    public String toString(){
        return idNumber + " " + firstname + " " + lastname;
    }
}

class Student extend Person{

    private int [] scores;
    
    public Student(String firstname, String lastname, long id, int [] scores){
    
        super(firstname, lastname, id);
        this.scores = scores;
    
    }
    
    public char CalculateScores(){
        
        int total = 0;
        
        for(int i = 0; i < scores.length; i++){
            total += scores[i];
        }
        
        int avg = total / scores.length;
        
        if(avg >= 90 && avg <= 100){
            return 'A';
        }
        if(avg >= 80 && avg <= 90){
            return 'B';
        }
        if(avg >= 70 && avg <= 80){
            return 'C';
        }
        if(avg >= 50 && avg <= 70){
            return 'D';
        }
        
        return 'F';
    
    }
}

public class Person2{

    private static Scanner sc = new Scanner(System.in);
    
    public static void main(String [] args){
    
        String firstName = sc.next();
        String lastName = sc.next();
        int id = sc.nextInt();
        int numScores = sc.nextInt();
        int[] testScores = new int[numScores];
        for (int i = 0; i < numScores; i++) {
            testScores[i] = sc.nextInt();
        }
        sc.close();

        Student s = new Student(firstName, lastName, id, testScores);
        System.out.println(s.toString());
        System.out.println("Grade: " + s.calculate());
    
    }

}



