abstract class Shape{

    private String color;
    
    public Shape(String color){
        this.color = color;
    }
    
    abstract double area();
    
    public String getColor(){
        return this.color;
    }
    
    abstract String toString();
    
}

class Circle extend Shape{

    private double radius;
    private final double PI = 3.14;
    
    public Circle(String color, double radius){
        super(color);
        this.radius = radius;  
    }
    
    @Override
    public double area(){
        return PI * radius * radius;
    }
    
    @Override
    public String toString(){
        return "Circle color is " + super.color + "and radius" + this.radius + "." + "Area of this circle" + area();
    }

}

class Rectangle extend Shape{
    
    private double height;
    private double width;
    
    public Rectangle(String color, double height, double width){
        super(color);
        this.height = height;
        this.width = width;
    }
    
    @Override
    public double area(){
        return this.height * this.width;
    }
    
    @Override
    public String toString(){
        return "Rectangle color is " + super.color + "and height and width [h: " + this.height + ", w: ]" + this.width +  "." + "Area of this rectangle" + area();
    }
}

public class  Polymorphism{

    public static void main(String [] args){
        
        Circle circle = new Circle("red", 3.0);
        System.out.println(circle.toString());
        Rectangle rectangle = new Rectangle("blue", 5.0, 4.0);
        System.out.println(rectangle.toString());
        
    }

}