interface AdvanceArithmetic{
    
    int DivisionSum(int n);
    int SumOfConsecutiveNumber(int n);
    double takeCos(double n);
    double takeSin(double n);
    int factorial(int n);

}

interface Arithmetic{
    
    double sum(double a, double b);
    double sub(double a, double b);
    double div(double a, double b);
    double mult(double a, double b);
    int mod(int a, int b);
    int pow(int a,int b);
    
}

class Calculator implements Arithmetic, AdvanceArithmetic{

    @Override
    int DivisionSum(int n){
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) sum += i;
        }
        return sum;
    }
    
    @Override
    public int SumOfConsecutiveNumber(int n){
        return (n*(n+1)) / 2;
    }
    
    @Override
    public double takeCos(double n){
        n = Math.toRadians(n);
        return Math.cos(n);
    }
    
    @Override
    public double takeCos(double n){
        n = Math.toRadians(n);
        return Math.sin(n);
    }
    
    @Override
    public int Factorial(int n){
        if (n == 1) return 1;
        return factorial(n - 1) * n;
    }
    
    @Override
    public double Sum(double a, double b){
        return a + b;
    }
    
    @Override
    public double Sub(double a, double b){
        return a - b;
    }
    
    @Override
    public double Div(double a, double b){
        return a / b;
    }
    
    @Override
    public double Mult(double a, double b){
        return a * b;
    }
    
    @Override
    public int Mod(int a, int b){
        return a % b;
    }
    
    @Override
    public int Pow(double a,double b){
        return Math.pow(a,b);
    }
}

public class Polymorphism2{
    
    public static void main(String [] args){
        Calculator cal = new Calculator();
        System.out.println(cal.Factorial(6));
    }
}